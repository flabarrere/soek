#include <stdio.h>
#include "demo.h"

void testDemo(int *nbTestSucces, int *nbTestTotal)
{
	*nbTestSucces = *nbTestTotal = 0;
	
	int succes;

	succes = sayHelloWorld(1)==0 ;
	printf("sayHelloWorld( 1) retourne  0: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;
	
	succes = sayHelloWorld(0)==0 ;
	printf("sayHelloWorld( 0) retourne  0: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;
	
	succes = sayHelloWorld(-4)==-1 ;
	printf("sayHelloWorld(-4) retourne  -1: %s\n", succes ? "Oui" : "Non" );
	(*nbTestTotal)++;
	if( succes ) (*nbTestSucces)++ ;

}       
