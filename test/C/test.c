#include <stdio.h>
#include <stdlib.h>

/* You have to make yours functions declarations in this header */
#include "test.h"

int main( int argc, char **argv )
{
	
	void (*testFunctions[])(int*,int*) =
		{ testResultArray, testConfig, testAudio }; /*< You can add your own test function */
	


	int nbTestSucces=0, nbTestTotal=0;
	int sumTestSucces=0 , sumTestTotal=0;
	int i, nbFunctions = 3; /*< Dont forget to change the number of test */
	for( i=0 ; i<nbFunctions ; i++ )
	{
		printf("\n=======================================\n");
		printf("DEBUT TEST %d\n\n", i+1 );
		testFunctions[i]( &nbTestSucces, &nbTestTotal );
		printf("\nFIN TEST %d\n", i+1 );
		printf("\n%d succes sur %d\n", nbTestSucces, nbTestTotal ); 
		printf("\n=======================================\n");
		
		sumTestSucces += nbTestSucces ;
		sumTestTotal  += nbTestTotal ;
	}

	printf("\n\n%d/%d tests succes (%d%%)\n", sumTestSucces, sumTestTotal, 100*sumTestSucces/sumTestTotal );

	exit(0);
}
