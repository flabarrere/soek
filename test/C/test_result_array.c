#include <stdio.h>
#include <string.h>
#include "result_array.h"
#include "config.h"

void testResultArray( int *nbTestSucces, int *nbTestTotal ) {
	*nbTestSucces = 0;
	*nbTestTotal = 0;

	Result* r;


	//Test result
	(*nbTestTotal)++;
	printf("Test 1 : Initialisation d'un résultat : ");
	r = result_init("a", 10);
	if( strcmp(r->filePath, "a") == 0 && r->distance == 10) {
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}
	(*nbTestTotal)++;
	printf("Test 2 : Reinitialisation du résultat : ");
	r = result_init("b", 20);
	if( strcmp(r->filePath, "b") == 0 && r->distance == 20) {
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}

	printf("\n\n");

	//Test result_array
	ResultArray ra;
	int resultat;
	CONFIG.nbResult = 6;
	(*nbTestTotal)++;
	printf("Test 3 : Initialisation d'un ResultArray : ");
	ra = resultArray_init();
	if( ra.size == 0 && ra.sizeMax == CONFIG.nbResult && ra.tab != NULL) {
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}
	(*nbTestTotal)++;
	printf("Test 4 : ResultArray est vide = ");
	resultat = resultArray_isEmpty(ra);
	printf("%d : ", resultat);
	if( resultat == 1 ) {
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}
	(*nbTestTotal)++;
	printf("Test 5 : ResultArray est plein = ");
	resultat = resultArray_isFull(ra);
	printf("%d : ", resultat);
	if( resultat == 0 ) {
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}

	printf("\n");

	(*nbTestTotal)++;
	printf("Test 6 : Ajouter un résultat : ");
	resultArray_addResult(&ra, r);
	if( strcmp(ra.tab[0]->filePath, "b") == 0 && r->distance == 20) {
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}
	(*nbTestTotal)++;
	printf("Test 7 : ResultArray est vide = ");
	resultat = resultArray_isEmpty(ra);
	printf("%d : ", resultat);
	if( resultat == 0 ) {
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}
	(*nbTestTotal)++;
	printf("Test 8 : ResultArray est plein = ");
	resultat = resultArray_isFull(ra);
	printf("%d : ", resultat);
	if( resultat == 0 ) {
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}

	printf("\n");

	printf("Test 9 : Remplir ResultArray\n");
	resultArray_print(ra);

	printf("\n");

	(*nbTestTotal)++;
	for(int i=8; !resultArray_isFull(ra); i++) {
		r = result_init("c", i+10);
		resultArray_addResult(&ra, r);
	}
	printf("Afficher la ResultArray :\n");
	resultArray_print(ra);
	(*nbTestSucces)++;
	printf("OK\n");

	printf("\n");

	printf("Test 10 : Ajouter un Result aillant pour chemin ""a"" et pour distance 100 :\n");
	(*nbTestTotal)++;
	r = result_init("a", 100);
	resultArray_addResult(&ra, r);
	resultArray_print(ra);
	if(strcmp(ra.tab[5]->filePath, "c") == 0 && ra.tab[5]->distance == 22){
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}

	printf("Test 11 : Ajouter un Result aillant pour chemin ""d"" et pour distance 22 :\n");
	(*nbTestTotal)++;
	r = result_init("d", 22);
	resultArray_addResult(&ra, r);
	resultArray_print(ra);
	if(strcmp(ra.tab[5]->filePath, "c") == 0 && ra.tab[5]->distance == 22){
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}

	printf("Test 12 : Ajouter un Result aillant pour chemin ""e"" et pour distance 19 :\n");
	(*nbTestTotal)++;
	r = result_init("e", 19);
	resultArray_addResult(&ra, r);
	resultArray_print(ra);
	if(strcmp(ra.tab[2]->filePath, "e") == 0 && ra.tab[2]->distance == 19){
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}

	printf("Test 13 : Ajouter un Result aillant pour chemin ""f"" et pour distance 10 :\n");
	(*nbTestTotal)++;
	r = result_init("f", 10);
	resultArray_addResult(&ra, r);
	resultArray_print(ra);
	if(strcmp(ra.tab[0]->filePath, "f") == 0 && ra.tab[0]->distance == 10){
		(*nbTestSucces)++;
		printf("OK\n");
	} else {
		printf("NOK\n");
	}	
}