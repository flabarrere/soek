#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"

void testConfig( int *nbTestSucces, int *nbTestTotal ) {
	(*nbTestSucces) = 0;
	(*nbTestTotal) = 0;

	system("cp test/soek.config ./data");


	printf("Test 1 : Chargement de configuration prédéfinie\n");
	(*nbTestTotal) ++;

	config_loadFile();

	if( CONFIG.nbResult == 42 && CONFIG.nbEmergenceMin == 69 && CONFIG.nbCharacterMin == 666 && CONFIG.nbWordMax == 67) {
		printf("OK\n\n");
		(*nbTestSucces) ++;
	} else {
		printf("NOK\n\n");
	}

	
	printf("Test 2 : Sauvegarde de nouveaux paramètres\n");
	(*nbTestTotal) ++;

	CONFIG.nbResult = 1;
	CONFIG.nbEmergenceMin = 3;
	CONFIG.nbCharacterMin = 4;
	CONFIG.nbWordMax = 5;

	if(!config_saveFile()) {
		printf("OK\n\n");
		(*nbTestSucces) ++;
	} else {
		printf("NOK\n\n");
	}


	printf("Test 3 : Chargement des nouveaux paramètres\n");
	(*nbTestTotal) ++;

	if( CONFIG.nbResult == 1 && CONFIG.nbEmergenceMin == 3 && CONFIG.nbCharacterMin == 4 && CONFIG.nbWordMax == 5) {
		printf("OK\n\n");
		(*nbTestSucces) ++;
	} else {
		printf("NOK\n\n");
	}



	system("rm data/soek.config");
}