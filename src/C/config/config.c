/**@file config.c
 * 
 * @brief Configuration management
 * @author MALGOUYRES Alexandre
 * @date 19/12/2019
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** @var parameterName[]
 *
 *  @brief array whitch settings name
 */
char *parameterName[] = {"Result number", "Text maximum distance", "audio maximum distance", "RGB maximum distance", "Black and white maximum distance","Taille des fenettres d'echantillonage","Nombre d'intervale de l'echantillonage","Seuil de tolerence pour que l'echantillon audio soit considéré", "Minimum emergence number", "Minimum number character", "Maximum number word"};

/** @var parameterValue[]
 *
 *  @brief array whitch pointers of settings value
 */
int *parameterValue[] = {&CONFIG.nbResult, &CONFIG.textDistanceMax, &CONFIG.audioDistanceMax, &CONFIG.rgbDistanceMax, &CONFIG.bwDistanceMax, &CONFIG.splitSize, &CONFIG.nbInterval, &CONFIG.threshold, &CONFIG.nbEmergenceMin, &CONFIG.nbCharacterMin, &CONFIG.nbWordMax};

/** @var defaultValue[]
 *
 *  @brief array whitch defaul setting value
 */
int defaultValue[] = {10, 5000, 30000, 30000, 30000, 1024, 16, 256, 5, 5, 20};


int config_loadSetting(FILE* f, char* setting, int *d){
    char buffer[500];
    char c[200] = "\n";
    strcat(c, setting);
    strcat(c, " : %d");

    rewind(f);

    while(!feof(f)){
        if(fscanf(f, c, d)){
            return 0;
        } else {
            fscanf(f,"%s",buffer);
        }
    }
    return 1;
}



int config_loadFile(){
    int error = 0;
    FILE *f = NULL;
    int paraterNb = sizeof(parameterName)/sizeof(long);

    f = fopen("data/soek.config", "r");
    if(f == NULL) {
        for(int i=0 ; i<paraterNb ; i++) {
            *parameterValue[i] = defaultValue[i];
        }
        config_saveFile();
        return 1;
    }

    for(int i = 0; i<paraterNb; i++) {
        if( config_loadSetting(f, parameterName[i], parameterValue[i]) ) {
            *parameterValue[i] = defaultValue[i];
            error = 2;
        }
        if((i==10 && *parameterValue[i]<-1) || (i<10 && *parameterValue[i]<0)) {
            *parameterValue[i] = -*parameterValue[i];
            error = 4;
        }
    }

    if(error == 2) {
        if(config_saveFile()) {
            error = 3;
        }
    }

    fclose(f);
    return 0;

}



int config_saveFile() {
    int paraterNb = sizeof(parameterName)/sizeof(long);

    remove("data/soek.config");

    FILE* f = NULL;
    f = fopen("data/soek.config", "a+");

    if(f == NULL) return 1;

    for(int i=0; i<paraterNb; i++) {
        fprintf(f, "%s : %d\n", parameterName[i], *parameterValue[i] );
    }    

    fclose(f);
    return 0;
}