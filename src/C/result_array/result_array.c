/**@file result_array.c
 * 
 * @brief Type used to store research results
 * @author MALGOUYRES Alexandre
 * @date 07/12/2019
 */

#include "result_array.h"
#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Result* result_init(char* filePath, int distance) {
    Result* r;
	r = (Result*)malloc(sizeof(Result));
	r->filePath = filePath;
	r->distance = distance;
	return r;
}




ResultArray resultArray_init() {
	ResultArray r;
	r.size = 0;
	r.sizeMax = CONFIG.nbResult;
	r.tab = (Result**)malloc(r.sizeMax *sizeof(Result*));
	return r;
}


int resultArray_isEmpty(ResultArray r) {
	return r.size == 0;
}

int resultArray_isFull(ResultArray r) {
	return r.size >= r.sizeMax;
}

void resultArray_addResult (ResultArray* r, Result* my_result) {
	for(int i=0; i<r->size; i++) {

		//Vérifie si l'on doit insérer le nouveau résultat
		if(r->tab[i]->distance > my_result->distance) {
			int limite;
			if(resultArray_isFull(*r)) {
				limite = r-> size-1;
			} else {
				limite = r-> size;
				r->size++;
			}

			//Décale tous les résultats pour pouvoir insérer le nouveau
			for(int j=limite; j>i; j--) {
				r->tab[j] = r->tab[j-1];
			}

			r->tab[i] = my_result;

			return;
		}
	}

	//Si on n'a pas pu placer le nouveau résultat avant et que le tableau n'est pas plein on le place à la fin du tableau
	if(!resultArray_isFull(*r)) {
		r->tab[r->size] = my_result;
		r->size++;
	}
}

Result* resultArray_getResult(ResultArray r, int index) {
	if(index>r.size-1) return NULL;
	return r.tab[index];
}


void resultArray_print(ResultArray r){
	for (int i=0; i<r.size; i++) {
		printf("Fichier : %s (distance = %d)\n", resultArray_getResult(r,i)->filePath, resultArray_getResult(r,i)->distance);
	}
}

void resultArray_clear(ResultArray r) {
	for( int i = r.size -1 ; i >= 0 ; i-- ) {
		free(r.tab[i]);
	}
	r.size = 0;
}
