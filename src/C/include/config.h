/** @file config.h
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <stdio.h>

/** @var CONFIG
 *
 *  @brief CONFIG is a global struct witch contain all setting value of soek
 */
struct{
	unsigned int nbResult;                      /**< Nombre de résultat voulu par l'utilisateur */
	unsigned int textDistanceMax; 	        	/**< Seuil de similitude pour les textes*/
    unsigned int audioDistanceMax; 	          	/**< Seuil de similitude pour les audios*/
    unsigned int rgbDistanceMax; 	        	/**< Seuil de similitude pour les RGB*/
    unsigned int bwDistanceMax; 	       		/**< Seuil de similitude pour les noir et blanc*/
    unsigned int splitSize;						/**< la taille des fenettres audion*/
    unsigned int nbInterval;					/**< nombre d'intevale de l'echantillonage audion */
    unsigned int threshold;						/**< seuil de tolerence pour que lechantillon soit considéré*/
    unsigned int nbEmergenceMin;                /**< Fréquence minimum du mot dans un texte */
    unsigned int nbCharacterMin;           	    /**< Le nombre minimum de lettres pour prendre un mot en compte */
    int nbWordMax;                              /**< Le nombre de mots clef maximal (-1 = pas de limite) */
} CONFIG;

/** @fn config_loadSetting(FILE* f, char* setting, int *d)
 *
 *  @brief Load a config setting from soek.config
 *	@param f pointer of config file
 *  @param setting setting to be read
 *	@param d setting value
 *  @return 0 if alright
 *  @return 1 if the setting can't be fond in soek.config
 */
 int config_loadSetting(FILE* f, char* setting, int *d);

/** @fn config_loadFile()
 *
 *  @brief Load CONFIG struct from soek.config
 *	@return 0 if alright
 *	@return 1 if config.soek didn't exist (soek.config is created)
 *	@return 2 if missing a setting in soek.config (setting get default value)
 *	@return 3 if missing a setting in soek.config and it can't be saved in soek.config
 *	@return 4 if a setting got a wrong value
 */
int config_loadFile();

/** @fn config_saveFile();
 *
 *  @brief Save CONFIG struct in soek.config
 *	@return 0 if alright
 *  @return 1 if soek.config can't be open and created
 */
int config_saveFile();

#endif
