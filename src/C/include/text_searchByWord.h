/** @file text_searchByWord.h
 */

#ifndef TEXT_SEARCHBYWORD_H
#define TEXT_SEARCHBYWORD_H 
#include "result_array.h"

/** @struct andWordSearch
 *	
 *	@see WordSearch
 */
/** @typedef WordSearch
 *
 *  @brief This structure contains ID with the number of instance a pointer to the next element
 */
typedef struct andWordSearch{
	int nb_instance;               /**< Contains the number of instance */
	int ID;                        /**< Contains the ID 0*/
	struct andWordSearch * next;   /**< Contains a pointer to the next element */
}WordSearch;

/** @struct andWordListSearch
 * 
 *  @see WordListSearch
 */
/** @typedef WordListSearch
 *
 *  @brief This structure contains a pointer to the first element of the WordSearch structure, it represents a list with ID and number of instance
 */
typedef struct andWordListSearch{
	WordSearch *headWordList;     /**< Contains the pointer to first element of the WordSearch structure */
}WordListSearch;

/** @fn wordListSearch_add( WordListSearch *list,int current_ID, int current_nb_instance )
 *
 *  @brief Add at the bottom of the WordListSearch an ID with a number of instance
 *  @param list The list of WordListSearch
 *  @param current_ID The current file ID
 *  @param current_nb_instance The current number of instance of the word (associated with ID)
 *  @see WordListSearch
 */
void wordListSearch_add(WordListSearch *list,int current_ID, int current_nb_instance);

/** @fn wordListSearch_init()
 *  
 *  @brief Initialize the struct andWordSearch and the struct andWordListSearch
 *	@return Pointer on the new WordListSearch
 *  @see WordListSearch
 */
WordListSearch *wordListSearch_init();

/** @fn wordListSearch_sort_list( WordListSearch *list )
 *
 *  @brief Sorts the WordListSearch in descending order of number of occurrences
 *  @param list The list of WordListSearch 
 *  @see WordListSearch
 */
void wordListSearch_sort_list(WordListSearch *list);

/** @fn generate_cmp_result( const char *wordToCompare,const char *userWord ) 
 *
 *  @brief Generates a score to take into account or not a current word (the corresponding ID)
 *  @param wordToCompare The word in liste_base_descripteur to compare with userWord
 *  @param userWord The search word desired by the user
 *  @return A score (multiple of 100 for additional letters, unit for changing letters, -1 if first letter is different)
 */
int generate_cmp_result(const char *wordToCompare,const char *userWord);

/** @fn generate_score( WordListSearch *list, ResultArray *files )
 *
 *  @brief Generates the similarity score and fills in the Result_Array structure for the word chosen by the user
 *  @param list The list of WordListSearch
 *  @param files ResultArray to fill with result of the search
 *  @see ResultArray
 */
void generate_score(WordListSearch *list, ResultArray *files);


#endif
