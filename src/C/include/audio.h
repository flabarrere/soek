/** @file audio.h
 */

#ifndef AUDIO_H
#define AUDIO_H

#include "result_array.h"

/** @fn audio_indexAll()
 * 
 *  @brief Index all audio file from data/audio
 *  @return 0 if issue, 1 else
 */
int audio_indexAll();

/** @fn audio_index( const char *filePath )
 * 
 *  @brief Index the audio file named filePath
 *  @param filePath Name of the file to index
 *  @return 0 if issue, 1 else
 */
int audio_index( const char *filePath );

/** @fn audio_searchByFile( const char *filePath, ResultArray *files )
 * 
 *  @brief Search the nearest audio files to the file named filePath and return them in the ResultArray
 *  @param filePath Name of the file to compare
 *  @param files ResultArray to fill with result of the search
 *  @return 0 if issue, 1 else
 *  @see ResultArray
 */
int audio_searchByFile( const char *filePath, ResultArray *files );

/** @fn audio_play( const char *filePath )
 * 
 *  @brief Play the audio file with an external program
 *  @param filePath Name of the file to play
 *  @return 0 if issue, 1 else
 */
int audio_play( const char *filePath );

#endif
