/** @file text.h
 */

#ifndef TEXT_H
#define TEXT_H

#include "result_array.h"

/** @fn text_indexAll()
 * 
 *  @brief Index all text file from data/text
 *  @return 0 if issue, 1 else
 */
int text_indexAll();

/** @fn text_index( const char *filePath )
 * 
 *  @brief Index the text file named filePath
 *  @param filePath Name of the file to index
 *  @return 0 if issue, 1 else
 */
int text_index( const char *filePath );

/** @fn text_searchByFile( const char *filePath, ResultArray *files )
 * 
 *  @brief Search the nearest text files to the file named filePath and return them in the ResultArray
 *  @param filePath Name of the file to compare
 *  @param files ResultArray to fill with result of the search
 *  @return 0 if issue, 1 else
 *  @see ResultArray
 */
int text_searchByFile( const char *filePath, ResultArray *files );

/** @fn text_searchByWord( const char *word, ResultArray *files )
 * 
 *  @brief Search the text files by content word and return them in the files
 *  @param word Word to search
 *  @param files ResultArray to fill with result of the search
 *  @return 0 if issue, 1 else
 *  @see ResultArray
 */
int text_searchByWord( const char *word, ResultArray *files );

/** @fn text_play( const char *filePath )
 * 
 *  @brief Play the text file with an external program
 *  @param filePath Name of the file to play
 *  @return 0 if issue, 1 else
 */
int text_play( const char *filePath );

#endif
