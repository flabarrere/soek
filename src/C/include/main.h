#ifndef MAIN_H
#define MAIN_H

/** @file main.h
 *
 * @brief File which contain the main function
 * @see main()
 */


/**@fn int main()
 *
 * @brief Main program function
 *
 * @return 0 if all right.
 */

int main();





#endif
