/** @file image_bw.h
 */

#ifndef IMAGE_BW_H
#define IMAGE_BW_H

#include "result_array.h"

/** @fn imageBW_indexAll()
 * 
 *  @brief Index all black and white images file from data/imageBW
 *  @return 0 if issue, 1 else
 */
int imageBW_indexAll();

/** @fn imageBW_index( const char *filePath )
 * 
 *  @brief Index the black and white image named filePath
 *  @param filePath Name of the file to index
 *  @return 0 if issue, 1 else
 */
int imageBW_index( const char *filePath );

/** @fn imageBW_searchByFile( const char *filePath, ResultArray *files )
 * 
 *  @brief Search the nearest black and white image to the image named filePath and return them in the ResultArray
 *  @param filePath Name of the file to compare
 *  @param files ResultArray to fill with result of the search
 *  @return 0 if issue, 1 else
 *  @see ResultArray
 */
int imageBW_searchByFile( const char *filePath, ResultArray *files );

/** @fn imageBW_play( const char *filePath )
 * 
 *  @brief Show the black and white image file with an external program
 *  @param filePath Name of the file to play
 *  @return 0 if issue, 1 else
 */
int imageBW_play( const char *filePath );

#endif
