/** @file text_index.h
 */

#ifndef TEXT_INDEX_H
#define TEXT_INDEX_H


/** @struct andWord
 *	
 *	@see Word
 */
/** @typedef Word
 *
 *  @brief This structure contains words : each word with its number of instance and a pointer to the next Word
 */
typedef struct andWord{
	int nb_instance;               /**< Contains the number of instance */
	char *word;                    /**< Contains a word */
	struct andWord * next;         /**< Contains a pointer to the next Word */
}Word;

/** @struct anWordList
 * 
 *  @see WordList
 */
/** @typedef WordList
 *	
 *  @brief This structure contains a pointer to the first element of the Word structure, it represents a list of word
 */
typedef struct andWordList{
	Word *headWordList;            /**< Contains the pointer to first element of the Word structure */
}WordList;

/** @fn wordList_print( WordList *list ) 
 * 
 *  @brief Print the list of Word for one file
 *  @param list The list of Word
 *  @see WordList
 */
void wordList_print( WordList *list );

/** @fn wordList_add( WordList *list, char *currentWord ) 
 * 
 *  @brief Add at the bottom of the WordList a single word
 *  @param list The list of Word
 *	@param currentWord The current word read from the file
 *  @see WordList
 */
void wordList_add( WordList *list, char *currentWord );

/** @fn wordList_increase_instance( WordList *list, char *currentWord ) 
 * 
 *  @brief Runs through the list of Word : if a word is already present increases the number of instance otherwise adds the word to the list of Word 
 *  @param list The list of Word
 *	@param currentWord The current word read from the file
 *  @see WordList
 */
void wordList_increase_instance( WordList *list, char *currentWord );

/** @fn wordList_init()
 * 
 *  @brief Initialize the struct andWord and the struct andWordList
 *	@return Pointer on the new WordList
 *  @see WordList
 */
WordList *wordList_init();

/** @fn wordList_sortDescending( WordList *list )
 *
 *  @brief Sorts the Word list in descending order of number of occurrences
 *  @param list The list of Word
 *  @see WordList
 */
void wordList_sortDescending(WordList *list);

/** @fn genererate_selected_wordCount( WordList *list, unsigned int nbEmergenceMin, int nbWordMax,int list_word_count )
 *
 *  @brief Generates the selected word (keywords) count according to the configuration parameters
 *  @param list The list of Word
 *  @param nbEmergenceMin The minimum number of occurrences of a word (configuration parameter)
 *  @param nbWordMax The maximum number of keywords (-1 = no limit) (configuration parameter)
 *  @param list_word_count The number of keywords contained in the Word list
 *  @return The selected word count
 */
int genererate_selected_wordCount(WordList *list, unsigned int nbEmergenceMin, int nbWordMax,int list_word_count);

/** @fn genererate_article_wordCount( WordList *list )
 *  
 *  @brief Generates the current article word count 
 *  @param list The list of Word
 *  @return The article word count
 */
int genererate_article_wordCount(WordList *list);

/** @fn genererate_list_wordCount( WordList *list )
 *  
 *  @brief Generates the number of keywords contained in the Word list
 *  @param list The list of Word
 *  @return The number of keywords in the list
 **/
int genererate_list_wordCount(WordList *list);

/** @fn genererate_ID( const char *fileName )
 *  
 *  @brief Generates the ID of the current file
 *  @param fileName The name of the corresponding file
 *  @return The ID
 */
int genererate_ID(const char *fileName);

/** @fn genenerate_base_descripteur_texte( int ID,int selected_word_count, WordList *list )
 *
 *  @brief Generates the file base_descripteur_texte that contains the descriptors of all the files contained in the database.
 *  @param ID The current file ID
 *  @param selected_word_count The selected word (keywords) count according to the configuration parameters
 *  @param list The list of Word
 */
void genenerate_base_descripteur_texte(int ID,int selected_word_count, WordList *list);

/** @fn genenerate_liste_base_texte( int ID, int selected_word_count, int article_word_count, const char *fileName )
 *
 *  @brief Generates the file liste_base_texte which contains the names of the files present in the base, the associated IDs, the number of words retained and the number of words in the file 
 *  @param ID The current file ID
 *  @param selected_word_count The selected word (keywords) count according to the configuration parameters
 *  @param article_word_count The current article word count
 *  @param fileName The name of the corresponding file
 */
void genenerate_liste_base_texte(int ID, int selected_word_count, int article_word_count, const char *fileName);
										
	
#endif
