#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "result_array.h"
#include "audio.h"
#include "image_bw.h"
#include "image_rgb.h"
#include "text.h"
#include "useful.h"

void printSoek() {
	printf("                         \n");
	printf("█████  █████  █████  █  █\n");
	printf("█      █   █  █      █ █ \n");
	printf("█████  █   █  ███    ██  \n");
	printf("    █  █   █  █      █ █ \n");
	printf("█████  █████  █████  █  █\n");
}

int printTexteDescripteur(int id) {
	FILE *f = NULL;
	char c[20] = "\n", desc[500] = "";
	int idEnCours;
	char idChar[10];

	f = fopen("data/base_descripteur_texte", "r");
	if(f == NULL) return 1;

	sprintf(idChar,"%d",id);
	strcat(c, idChar);

	while(!feof(f)) {
		if(fscanf(f, "\n%d", &idEnCours)) {
			fscanf(f, " %[^\n]", desc);
			if(idEnCours == id) {
				printf("%s\n", desc);
			}
		} else {
			fscanf(f, "%[^\n]", desc);
		}
	}

	fclose(f);
	return 0;
}

int printAudioDescripteur(int id) {
	FILE *f = NULL;
	char c[20] = "\n", desc[500] = "";
	int idEnCours, fichierEnCours = 0;
	char idChar[10];

	f = fopen("data/base_descripteur_audio", "r");
	if(f == NULL) return 1;

	sprintf(idChar,"%d",id);
	strcat(c, idChar);

	while(!feof(f)) {
		if(fscanf(f, "\n#%d", &idEnCours)) {
			if(idEnCours == id) {
				fichierEnCours = 1;
			} else if(fichierEnCours) {
				fichierEnCours = 0;
				return 0;
			}
			fscanf(f, "\n%[^\n]", desc);
		} else {
			fscanf(f, "\n%[^\n]", desc);
		}
		if(fichierEnCours) {
			printf("%s\n", desc);
		}
	}

	fclose(f);
	return 0;
}

int printBWDescripteur(int id) {
	char command[100] = "cat < data/image_nb/base_descripteur_image_nb/", aux[50];

	sprintf(aux, "%d", id);

	strcat(command, aux);
	strcat(command, ".txt");

	system(command);

	printf("\n");
}

int printRGBDescripteur(int id) {
	char command[100] = "cat < data/image_rgb/base_descripteur_image_rgb/", aux[50];

	sprintf(aux, "%d", id);

	strcat(command, aux);
	strcat(command, ".txt");

	system(command);

	printf("\n");
}

void printAllTexteDescripteur() {
	FILE *f = NULL;
	int id;
	char fileName[500], buffer[500];

	f = fopen("data/liste_base_texte", "r");

	if(f == NULL) return;

	while(!feof(f)) {
		if(fscanf(f, "\n%d", &id) == 1) {
			fscanf(f, " %[^ ]", fileName);
			getFileName(fileName);
			printf("\n%s\n", fileName);
			printTexteDescripteur(id);
		}
		fscanf(f, "%[^\n]", buffer);
	}

	fclose(f);
}

void printAllAudioDescripteur() {
	FILE *f = NULL;
	int id;
	char fileName[500], buffer[500];

	f = fopen("data/liste_base_audio", "r");

	if(f == NULL) return;

	while(!feof(f)) {
		if(fscanf(f, "\n%d", &id) == 1) {
			fscanf(f, " %[^\n]", fileName);
			getFileName(fileName);
			printf("\n%s\n", fileName);
			printAudioDescripteur(id);
		} else {
			fscanf(f, "%[^\n]", buffer);
		}
	}

	fclose(f);
}

void printAllRGBDescripteur() {
	FILE *f = NULL;
	int id;
	char fileName[500], buffer[500];

	f = fopen("data/liste_base_image_rgb", "r");

	if(f == NULL) return;

	while(!feof(f)) {
		if(fscanf(f, "\n%d", &id) == 1) {
			fscanf(f, " %[^\n]", fileName);
			getFileName(fileName);
			printf("\n%s\n", fileName);
			printRGBDescripteur(id);
		} else {
			fscanf(f, "%[^\n]", buffer);
		}
	}

	fclose(f);
}

void printAllBWDecripteur() {
	FILE *f = NULL;
	int id;
	char fileName[500], buffer[500];

	f = fopen("data/liste_base_image_nb", "r");

	if(f == NULL) return;

	while(!feof(f)) {
		if(fscanf(f, "\n%d", &id) == 1) {
			fscanf(f, " %[^\n]", fileName);
			getFileName(fileName);
			printf("\n%s\n", fileName);
			printBWDescripteur(id);
		} else {
			fscanf(f, "%[^\n]", buffer);
		}
	}

	fclose(f);
}

int printAllDescripteur() {
	printf("\nTexte\n");
	printAllTexteDescripteur();
	printf("\n--------------------------------------------\n");
	printf("\nAudio\n");
	printAllAudioDescripteur();
	printf("\n--------------------------------------------\n");
	printf("\nImage RGB\n");
	printAllRGBDescripteur();
	printf("\n--------------------------------------------\n");
	printf("\nImage noir et blanc\n");
	printAllBWDecripteur();
}

void printHelpIndexer() {
	printf("<chaine> : indexe le fichier correspondant a la chaine.\n");
	printf("tout\t : indexe tout les fichiers contenus dans data.\n");
}

int indexer() {
	int (*index[])(const char *filePath) = { text_index, audio_index, imageRGB_index, imageBW_index };
	int (*indexAll[])() = { text_indexAll, audio_indexAll, imageRGB_indexAll, imageBW_indexAll };
	char filePath[500] = "";
	Type fileType = INCONNU;
	FILE *f = NULL;
	int aide;

	do {
		aide = 0;

		printf("\nIndexer\n");
		printf("Quel est le chemin de votre fichier ?\n\n");

		char a; 
		while( (a=getchar())!='\n' && a!=EOF );
		scanf("%500s", filePath);
		//fgets(filePath, 500, stdin);
		//filePath[strlen(filePath)-1] = '\0';

		f = fopen(filePath, "r");
		if(f == NULL) {
			if(strcmp(filePath,"tout") == 0) {
				for(int i=0; i<4; i++) {
					(*indexAll[i])();
				}
				return 0;
			} else if(strcmp(filePath,"aide") == 0) {
				printf("\n");
				printHelpIndexer();
				aide = 1;
			} else {
				printf("\n\033[00;91mAttention ! \033[00mFichier introuvable !\n");
				return 1;
			}
		} else {
			fileType = getType(filePath);
			if(fileType != INCONNU) {
				(*index[fileType])(filePath);
			} else {
				printf("\n\033[00;91mAttention ! \033[00mType de fichier inconnu !\n");
			}		
		}

	} while( aide );

	return 0;
}

int home(int admin) {
	int res;
	printf("\nAccueil\n");
	printf("Que souhaitez-vous faire ?\n");
	if(admin) {
		printf("1) Rechercher un fichier\n2) Rechercher un mot\n3) Paramètres administrateur\n4) Quitter Soek\n\n");
		
		scanf("%d", &res);
		
		if(res == 1 || res == 2 || res == 3) return res;
		return -1;
	} else {
		printf("1) Rechercher avec un fichier\n2) Rechercher avec un mot\n3) Quitter Soek\n\n");

		scanf("%d", &res);

		if(res == 1 || res == 2) return res;
		return -1;
	}
}

int search() {
	int res;
	char word[100] = "";
	ResultArray result = resultArray_init();

	printf("\nRecherche avec un mot\n");
	printf("Quel est votre mot ?\n\n");

	char a; while( (a=getchar())!='\n' && a!=EOF );
	fgets(word, 100, stdin);
	word[strlen(word)-1] = '\0';

	text_searchByWord(word, &result);

	if(resultArray_isEmpty(result)) {
		printf("\nAucun fichier ne correspond à votre recherche.\n");
	}else{
		printf("\nVoici quelques fichiers semblables à %s :\n", word);
		resultArray_print(result);
		text_play(resultArray_getResult (result, 0)->filePath);
	}

	resultArray_clear(result);
	return 0;
}

int compare() {
	int res;
	char filePath[500] = "";
	char c[600] = " ";
	ResultArray result = resultArray_init();
	FILE *f = NULL;

	int (*index[])(const char *filePath) = { text_index, audio_index, imageRGB_index, imageBW_index };
	int (*searchByFile[])(const char *filePath, ResultArray *files) = { text_searchByFile, audio_searchByFile, imageRGB_searchByFile, imageBW_searchByFile };
	int (*play[])(const char *filePath) = { text_play, audio_play, imageRGB_play, imageBW_play };

	printf("\nRecherche avec un fichier\n");
	printf("Quel est le chemin de votre fichier ?\n\n");

	char a; while( (a=getchar())!='\n' && a!=EOF );
	fgets(filePath, 500, stdin);
	filePath[strlen(filePath)-1] = '\0';

	f = fopen(filePath, "r");
	if(f == NULL) {
		printf("\n\033[00;91mAttention ! \033[00mFichier introuvable !\n");
		return 0;
	}
	fclose(f);

	Type fileType = getType(filePath);

	if(fileType == INCONNU) {
		printf("\n\033[00;91mAttention ! \033[00mType de fichier non taiter par Soek !\n");
		return 0;
	}

	if(isIndexed(filePath) == 0) {
		(*index[fileType])(filePath);
	}

	strcpy(c, filePath);

	getIndexedPath(c);
	(*searchByFile[fileType])(c, &result);

	if(resultArray_isEmpty(result)) {
		printf("\nAucun fichier ne correspond à votre recherche.\n");
	}else{
		printf("\nVoici quelques fichiers semblables à %s :\n", filePath);
		resultArray_print(result);
		(*play[fileType])(resultArray_getResult (result, 0)->filePath);
	}

	resultArray_clear(result);
	return 0;
}

void printConfig(int parameterValue[]) {
	char *parameterName[] = {"Nombre de résultat", "Distance maximale des textes", "Distance maximale des audios", "Distance maximale des images RGB", "Distance maximale des images noir et blanc", "Taille des fenetres d'echantillonage","Nombre d'intervale de l'echantillonage","Seuil de tolerance pour que l'echantillon audio soit considere", "Nombre minimum de caractere par mot", "Nombre minimum d'occurrence des mots", "Nombre maximum de mots par descripteur"};
	int paraterNb = sizeof(parameterName)/sizeof(long);
	for(int i = 0 ; i < paraterNb ; i++) {
		printf("%d) %s : %d\n", i+1, parameterName[i], parameterValue[i]);
	}
}

void configurer() {
	int res;
	int parameterValue[] = {CONFIG.nbResult, CONFIG.textDistanceMax, CONFIG.audioDistanceMax, CONFIG.rgbDistanceMax, CONFIG.bwDistanceMax, CONFIG.splitSize, CONFIG.nbInterval, CONFIG.threshold, CONFIG.nbCharacterMin, CONFIG.nbEmergenceMin, CONFIG.nbWordMax};

	printf("\nConfigurer\n");
	printConfig(parameterValue);
	printf("12) Annuler\n\n");
	scanf("%d", &res);

	while(res != 12) {
		if(res >= 1 && res <= 11) {
			int val = 0;
			printf("\nQuelle doit être sa valeur ?\n\n");
			scanf("%d", &val);
			if(res == 10 && val < -1) {
				parameterValue[res-1] = -val;
			} else if(val < 0) {
				parameterValue[res-1] = -val;
			} else {
				parameterValue[res-1] = val;
			}

		}else if(res == 13){
			int *trueParameterValue[] = {&CONFIG.nbResult, &CONFIG.textDistanceMax, &CONFIG.audioDistanceMax, &CONFIG.rgbDistanceMax, &CONFIG.bwDistanceMax, &CONFIG.splitSize, &CONFIG.nbInterval, &CONFIG.threshold, &CONFIG.nbCharacterMin, &CONFIG.nbEmergenceMin, &CONFIG.nbWordMax};

			int paraterNb = 11;
			for(int i = 0 ; i < paraterNb ; i++) {
				*trueParameterValue[i] = parameterValue[i];
			}
			if (config_saveFile()) {
				printf("\033[00;91mErreur\033[00m, impossible de sauvegarder les parmètres\n");
				exit(EXIT_FAILURE);
			}
			audio_indexAll();
			return;
		}
		printConfig(parameterValue);
		printf("12) Annuler\n13) Valider\n\n");
		scanf("%d", &res);
	}
}

void printHelpDescripteur() {
	printf("<chaine> : affiche le descripteur correspondant à la chaine.\n");
	printf("tout\t : affiche la totalité des descripteurs.\n");
	printf("texte\t : affiche tout les descripteurs de texte.\n");
	printf("audio\t : affiche tout les descripteurs d'audio.\n");
	printf("bw\t : affiche tout les descripteurs d'image en noir et blanc.\n");
	printf("rgb\t : affiche tout les descripteurs d'image en rgb.\n");
}

void printDescripteur() {
	char filePath[500] = "";
	Type fileType = INCONNU;
	int (*print[])(int id) = { printTexteDescripteur, printAudioDescripteur, printRGBDescripteur, printBWDescripteur};
	int aide;

	do {
		aide = 0;

		printf("\nVisualisation des descripteurs\n");
		printf("Quel est le nom de votre fichier ?\n\n");

		char a; while( (a=getchar())!='\n' && a!=EOF );
		scanf("%500s", filePath);
		//fgets(filePath, 500, stdin);
		//filePath[strlen(filePath)-1] = '\0';

		if(!isIndexed(filePath)) {
			if(strcmp(filePath, "tout") == 0) {
				printAllDescripteur();
				return;
			} else if(strcmp(filePath, "texte") == 0) {
				printAllTexteDescripteur();
				return;
			} else if(strcmp(filePath, "audio") == 0) {
				printAllAudioDescripteur();
				return;
			} else if(strcmp(filePath, "nb") == 0) {
				printAllBWDecripteur();
				return;
			} else if(strcmp(filePath, "rgb") == 0) {
				printAllRGBDescripteur();
				return;
			} else if(strcmp(filePath, "aide") == 0) {
				printf("\n");
				printHelpDescripteur();
				aide = 1;
			} else {
				printf("\n\033[00;91mAttention ! \033[00mCe document n'est pas indexé !\n");
				return;
			}
		}

		} while( aide );

	printf("\n");

	fileType = getType(filePath);

	if(fileType == INCONNU) {
		printf("\n\033[00;91mAttention ! \033[00mType de fichier inconnu !\n");
	}

	(*print[fileType])(getId(filePath));
}

int administration() {
	int res;
	printf("\nAdministrateur\n");
	printf("Que souhaitez-vous faire ?\n");
	printf("1) Indexer\n2) Configurer\n3) Visualiser descripteur\n4) Accueil\n5) Quitter Soek\n\n");
	scanf("%d", &res);

	switch(res) {
		case 1 :
			indexer();
			return 3;
		case 2 :
			configurer();
			return 3;
		case 3 :
			printDescripteur();
			return 3;
		case 4 :
			return 0;
		default :
			return -1;
	}
}

void printHelpAdmin() {
	printf("soek admin index  : lance le menu indexation de Soek.\n");
	printf("soek admin config : lance le menu configuration de Soek.\n");
	printf("soek admin desc   : lance le menu descripteur de Soek.\n");
}

void printHelpSoek() {
	printf("soek\t\t  : lance le logiciel Soek en mode classique.\n");
	printf("soek admin\t  : lance le logiciel Soek en mode administrateur.\n");
	printHelpAdmin();
}

void launch(int argc, char *argv[], int *admin) {
	if(checkDataFile()) {
		printf("\033[00;91mErreur\033[00m, pas de fichier ""data""\n");
		exit(EXIT_FAILURE);
	}
	*admin = argc > 1 && strcmp(argv[1], "admin") == 0;
	config_loadFile();
	if(argc > 1 && strcmp(argv[1], "aide") == 0) {
		printHelpSoek();
		exit(EXIT_SUCCESS);
	} else if( argc > 2 && strcmp( argv[1], "admin" ) == 0 && strcmp(argv[2], "index" ) == 0) {
		indexer();
		exit(EXIT_SUCCESS);
	} else if( argc > 2 && strcmp( argv[1], "admin" ) == 0 && strcmp(argv[2], "config" ) == 0) {
		configurer();
		exit(EXIT_SUCCESS);
	} else if( argc > 2 && strcmp( argv[1], "admin" ) == 0 && strcmp(argv[2], "desc" ) == 0) {
		printDescripteur();
		exit(EXIT_SUCCESS);
	} else if( argc > 2 && strcmp( argv[1], "admin" ) == 0 && strcmp(argv[2], "aide" ) == 0) {
		printHelpAdmin();
		exit(EXIT_SUCCESS);
	}
}

void main (int argc, char *argv[]) {
	int admin;
	int menu = 0;

	launch (argc, argv, &admin);
	printSoek();
	printf("\nBienvenue sur Soek, votre moteur de recherche local\n");

	while(1) {
		switch(menu) {
			case -1 :
				printf("\nA bientôt sur Soek !\n");
				exit(EXIT_SUCCESS);
			case 0 :
				menu = home(admin);
				break;
			case 1 :
				menu = compare();
				break;
			case 2 :
				menu = search();
				break;
			case 3 :
				menu = administration();
				break;
		}
	}
}
