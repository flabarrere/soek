/**@file text_index.c
 * 
 * @brief Specific function for the function text_index
 * @author guilhem
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "text_index.h"

//depreciated function
void wordList_print(WordList *list){
	Word *current=list->headWordList;
	printf("Affichage :\n");
	while (current!=NULL){
		printf("%s\t%d\n",current->word,current->nb_instance);
		current=current->next;
	}		
}

//add a word with number of instance to the structure
void wordList_add(WordList *list,char *currentWord){
	Word *newWord = malloc(sizeof(Word));
	if (newWord==NULL){
		exit(EXIT_FAILURE);
	}
	newWord->word = currentWord;
	newWord->nb_instance=1;
    newWord->next=list->headWordList;
    list->headWordList=newWord;
}

//if the word already exist then increases the number of instance by 1, otherwise add the new word with the wordList_add function
void wordList_increase_instance(WordList *list, char *currentWord){
    char *currentWordCpy = malloc(sizeof(char)*(strlen(currentWord)+1));
    strcpy(currentWordCpy,currentWord);
    int i;
    for (i=0;currentWordCpy[i]!='\0';i++){
        currentWordCpy[i]=tolower(currentWordCpy[i]);
    }
	Word *current =list->headWordList;
	int test=0;
	while (current!=NULL){
		if (strcmp(current->word,currentWordCpy)==0){
			current->nb_instance++;
			test=1;
		}
		current=current->next;
	}
	if(test==0){
		wordList_add(list,currentWordCpy);
	}
}

WordList *wordList_init(){
    WordList *list = malloc(sizeof(*list));
	list->headWordList=NULL;
	return list;
}

//sort by descending order of number of instance
void wordList_sortDescending(WordList *list){
    Word *new1=list->headWordList;
    Word *count;
    for (; new1->next!=NULL;new1=new1->next){
        for(count =new1->next; count !=NULL; count = count ->next){
            if (new1->nb_instance<count->nb_instance){
                int temp = new1->nb_instance;
                char *tempChar = new1->word;
                
                new1->nb_instance = count->nb_instance;
                new1->word = count ->word;
                
                count->nb_instance = temp;
                count->word = tempChar;
            }
        }
    }
}

//generate the number of word selected by the program with configuration parameter (we have to use list_word_count to know how many words - without number of instance - are in the structure
int genererate_selected_wordCount(WordList *list, unsigned int nbEmergenceMin, int nbWordMax,int list_word_count){
    Word *current=list->headWordList;
    int selected_word_count = 0;
    int i;
    int maxLoop = nbWordMax;
    
    //maxLoop<=0 : we take all the word in the structure with number of instance >= nbEmergenceMin
	if(maxLoop<=0){
        while (current!=NULL){
            if(current->nb_instance>=nbEmergenceMin){
                selected_word_count++;
            }
            current=current->next;
        }
	}
	else {
        //if the user want to take more words than there are in the structure
        if(nbWordMax>list_word_count){
            maxLoop=list_word_count;
        }
        //else 
        for(i=0;i<maxLoop;i++){
            if(current->nb_instance>=nbEmergenceMin){
                selected_word_count++;
            }
            current=current->next;
        }
    }
    return selected_word_count;
}

//count the number of words in the article (by the use of number of instance in the structure)
int genererate_article_wordCount(WordList *list){
    Word *current=list->headWordList;
    int article_word_count =0;
    
    while(current!=NULL){
        article_word_count+=current->nb_instance;
        current=current->next;
    }
    return article_word_count;
        
}

//count the number of word in the structure (just the words and not the number of instance)
int genererate_list_wordCount(WordList *list){
    Word *current=list->headWordList;
    int list_word_count=0;
    while(current!=NULL){
        list_word_count++;
        current=current->next;
    }
    return list_word_count;
        
}

//generate a single ID for a file, if the file already exist we return its own ID, else we return max_ID + 1 : an ID that does not exist yet in the file liste_base_texte
int genererate_ID(const char *fileName){
    int current_ID=0;
    int current_selected_word_count;
    int current_article_word_count;
    int max_ID=0;
    char current_fileName[1000];
    int cmp_result;
    
    FILE* file_liste_base=NULL;
    file_liste_base = fopen("data/liste_base_texte","a");
    fclose(file_liste_base);
    
    file_liste_base = fopen("data/liste_base_texte","r");
    if (file_liste_base!=NULL){
        while(!feof(file_liste_base)){
            fscanf(file_liste_base,"%d %s %d %d",&current_ID,current_fileName,&current_selected_word_count,&current_article_word_count);
            cmp_result=strcmp(current_fileName,fileName);
            if(current_ID>=max_ID){
                max_ID=current_ID;
            }
            if(cmp_result==0){
                return current_ID;
            }
            
        }
        fclose(file_liste_base);
    }
    max_ID++;
    return max_ID;
}

//generate the file base_descripteur_texte with the UNIX commands, we need the ID and selected_word_count
void genenerate_base_descripteur_texte(int ID,int selected_word_count, WordList *list){
    char base_descripteur_command[100000]={0};
    
    Word *current=list->headWordList;
    int i;
    
    FILE* file_base_desc=NULL;
    file_base_desc = fopen("data/base_descripteur_texte","a");
    fclose(file_base_desc);
    
    FILE* file_base_desc_cpy = NULL;
    file_base_desc_cpy = fopen("data/base_descripteur_texte_cpy", "w+");
    fclose(file_base_desc_cpy);
    
    int current_ID;
    char currentWord[100000];
    int current_nb_instance;
    
    file_base_desc = fopen("data/base_descripteur_texte","r");
    //we copy all the lines where the ID passed in parameter is not present (this allows to update the descriptor file if the configuration parameter have changed)
	if (file_base_desc!=NULL){
        while(!feof(file_base_desc)){
            fscanf(file_base_desc,"%d %s %d\n",&current_ID,currentWord,&current_nb_instance);
            if(!(current_ID==ID || current_ID==0)){
                sprintf(base_descripteur_command,"printf \"%d %s %d\n\" >> data/base_descripteur_texte_cpy",current_ID,currentWord,current_nb_instance);
                system(base_descripteur_command);
                base_descripteur_command[0]='\0';
            }
        }
        fclose(file_base_desc);
    }
    //then we add the selected word with ID to the descriptor file 
    for(i=0;i<selected_word_count;i++){
        sprintf(base_descripteur_command,"printf \"%d %s %d\n\" >> data/base_descripteur_texte_cpy",ID,current->word,current->nb_instance);
        system(base_descripteur_command);
        base_descripteur_command[0]='\0';
        current=current->next;
    }
    system("mv data/base_descripteur_texte_cpy data/base_descripteur_texte");
    
}

//generate the file liste_base_texte with the UNIX commands, we need the ID , selected_word_count and article_word_count
void genenerate_liste_base_texte(int ID, int selected_word_count, int article_word_count, const char *fileName){
    char liste_base_command[100000]={0};
    int cmp_result;
    
    int current_ID=0;
    int current_selected_word_count;
    int current_article_word_count;
    char current_fileName[1000];
    
    FILE* file_liste_base_cpy = NULL;
    file_liste_base_cpy = fopen("data/liste_base_texte_cpy", "w+");
    fclose(file_liste_base_cpy);
    
    FILE* file_liste_base=NULL;
    file_liste_base = fopen("data/liste_base_texte","r");
    
     //we copy all the lines where the ID passed in parameter is not present (this allows to update the text base file if the configuration parameter have changed)
    if (file_liste_base!=NULL){
        while(!feof(file_liste_base)){
            fscanf(file_liste_base,"%d %s %d %d\n",&current_ID,current_fileName,&current_selected_word_count,&current_article_word_count);
            cmp_result=strcmp(current_fileName,fileName);
            if(!((cmp_result==0 && current_ID==ID) || current_ID==0)){
                sprintf(liste_base_command,"printf \"%d %s %d %d\n\" >> data/liste_base_texte_cpy",current_ID,current_fileName,current_selected_word_count,current_article_word_count);
                system(liste_base_command);
                liste_base_command[0]='\0';
            }
        }
        fclose(file_liste_base);
    }
    //then we add the parameter to the text base file
    sprintf(liste_base_command,"printf \"%d %s %d %d\n\" >> data/liste_base_texte_cpy",ID,fileName,selected_word_count,article_word_count);
    system(liste_base_command);
    liste_base_command[0]='\0';
    
    system("mv data/liste_base_texte_cpy data/liste_base_texte");
}
