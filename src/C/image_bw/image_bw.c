/**@file image_bw.c
 * 
 * @brief Entity that manipulates black & white pictures
 * @author CLERMONT-PEZOUS Nicolas
 * @date 20/12/2019
 */

#include "image_bw.h"

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <useful.h>

#include <config.h>

int imageBW_indexAll(){
	char *ext;
	char fileName[100];
	char filePath[500];

	FILE * fichierDirectory = popen("ls data/image_nb", "r");

	//Read each file name from black and white image directory
	while(!feof(fichierDirectory)){
		fscanf(fichierDirectory,"%s",fileName);
    		ext = strrchr(fileName, '.');

		//Check if they are image (bmp) and index them
		if(ext != NULL && strcmp(ext+1,"bmp")==0){  		
			strcpy(filePath,"data/image_nb/");
			strcat(filePath,fileName);		
			imageBW_index(filePath);
		}
    	}
    	pclose(fichierDirectory);

	return 0;
}

int imageBW_index(const char *filePath){
	FILE *fichierTxt = NULL;
	FILE *fichierDesc = NULL;
	FILE *fichierListe = NULL;

	//Check if file referenced by filePath exist
	FILE *checkFile;
	checkFile = fopen(filePath, "r");
	if(checkFile == NULL){
        	return 1;
    	}
	fclose(checkFile);

	//Create descriptor directory (if it doesn't already exist)
	system("mkdir data/image_nb/base_descripteur_image_nb 2>/dev/null");
	
	//Create descriptor list (if it doesn't already exist)
	FILE *checkListe;
	checkListe = fopen("data/liste_base_image_nb", "a");
	if(checkListe == NULL){
		fprintf(stderr,"Fichier liste non créé!\n");
        	return 1;
    	}
	fclose(checkListe);

	char filePathInBase[500];
	strcpy(filePathInBase,filePath);
	
	//Check if file referenced by filePath is already indexed
	if(isIndexed(filePathInBase) == 1){
		return 0;	
	}

	getIndexedPath(filePathInBase);

	//Copy image file into black and white image base
	char command[500] = "cp ";
	strcat(command,filePath);
	strcat(command," data/image_nb 2>/dev/null");
	system(command);

	char txtFilePathInBase[500];
	strcpy(txtFilePathInBase,filePathInBase);
	int l = strlen(txtFilePathInBase);
	txtFilePathInBase[l-3] = '\0';
	strcat(txtFilePathInBase,"txt");

	char txtFilePath[500];
	strcpy(txtFilePath,filePath);
	l = strlen(txtFilePath);
	txtFilePath[l-3] = '\0';
	strcat(txtFilePath,"txt");

	//Copy image file .txt into black and white image base
	strcpy(command,"cp ");
	strcat(command,txtFilePath);
	strcat(command," data/image_nb 2>/dev/null");
	system(command);

	fichierListe = fopen("data/liste_base_image_nb","r+");

	if(fichierListe == NULL) {
		fprintf(stderr,"Impossible d'ouvrir le fichier liste\n");
		return 1;
	}

	int id = 0;
	char buffer[500];
	
	//Get last id from descriptor list
	while(!feof(fichierListe)){
		fscanf(fichierListe,"\n%d",&id);
		fscanf(fichierListe,"%s",buffer);

		//Double check if file referenced by file path isn't already indexed
		if(strcmp(filePath,buffer) == 0){
			fclose(fichierListe);
			return 0;
		}
	}

	id++;
	char idToString[10];
	sprintf(idToString,"%d",id);

	//Create file descriptor using id
	char filePathDesc[500] = "data/image_nb/base_descripteur_image_nb/";
	strcat(filePathDesc,idToString);
	strcat(filePathDesc,".txt");

	fichierDesc = fopen(filePathDesc,"w");

	if(fichierDesc == NULL) {
		fclose(fichierListe);
		fprintf(stderr,"Impossible d'ouvrir le fichier desc\n");
		return 1;
	}

	//Add file currently indexed into descriptor list
	fprintf(fichierListe,"%d ",id);
	fprintf(fichierListe,"%s\n",filePathInBase);

	int buff;
	int nbLi;
	int nbCol;
	int nbComp;
		
	fichierTxt = fopen(txtFilePath,"r");

	if(fichierTxt == NULL) {
		fclose(fichierListe);
		fclose(fichierDesc);
		fprintf(stderr,"Impossible d'ouvrir le fichier txt\n");
		return 1;
	}

	//Read image parameters (lines, colons and component number)
	fscanf(fichierTxt,"%d",&buff);
	nbLi = buff;
	fscanf(fichierTxt,"%d",&buff);
	nbCol = buff;
	fscanf(fichierTxt,"%d",&buff);
	nbComp = buff;

	//Read each pixel from image
	int tabDesc[2] = {0};
	for(int i=0;i<nbLi;i++){
		for(int j=0;j<nbCol;j++){
			fscanf(fichierTxt,"%d",&buff);
			if(buff == 0) tabDesc[0]++;
			else tabDesc[1]++;		
		}
	}

	//Write and increment the number of black or white pixels in file descriptor
	for(int i=0;i<2;i++){
		fprintf(fichierDesc,"%d\n",tabDesc[i]);	
	}
	
	fclose(fichierListe);
	fclose(fichierTxt);
	fclose(fichierDesc);

	return 0;
}

int imageBW_searchByFile(const char *filePath, ResultArray *files){
	char filePathInBase[500];
	strcpy(filePathInBase,filePath);

	//Check if file referenced by filePath is already indexed
	//If not, it starts the indexation
	if(!isIndexed(filePathInBase)){
		imageBW_index(filePathInBase);	
	}

	getIndexedPath(filePathInBase);

	FILE *fichierListe = NULL;
	FILE *fichierDesc = NULL;

	fichierListe = fopen("data/liste_base_image_nb","r+");

	if(fichierListe == NULL) {
		fprintf(stderr,"Impossible d'ouvrir le fichier liste\n");
		return 1;
	}

	int descValues[2];
	int indexId;
	char buffer[500];
	
	//For each file in descriptor list...
	while(!feof(fichierListe)){
		fscanf(fichierListe,"%d",&indexId);
		fscanf(fichierListe,"%s\n",buffer);

		//... find the descriptor for the image referenced by filePath...
		if(strcmp(filePathInBase,buffer) == 0){
			char descPath[500] = "data/image_nb/base_descripteur_image_nb/";
			char idToString[10];
			sprintf(idToString,"%d",indexId);

			strcat(descPath,idToString);
			strcat(descPath,".txt");	

			fichierDesc = fopen(descPath,"r");

			if(fichierDesc == NULL) {
				fclose(fichierListe);
				fprintf(stderr,"Impossible d'ouvrir le fichier descripteur\n");
				return 1;
			}
			
			//... and read its values
			fscanf(fichierDesc,"%d",&descValues[0]);
			fscanf(fichierDesc,"%d",&descValues[1]);

			fclose(fichierDesc);
		}
	}
	fclose(fichierListe);

	FILE *fichierListe2 = NULL;
	fichierListe2 = fopen("data/liste_base_image_nb","r+");

	if(fichierListe2 == NULL) {
		fprintf(stderr,"Impossible d'ouvrir le fichier liste\n");
		return 1;
	}

	//For each file in descriptor list...
	while(!feof(fichierListe2)){
		char *buff = malloc(sizeof(filePath));

		fscanf(fichierListe2,"%d",&indexId);
		fscanf(fichierListe2,"%s\n",buff);

		//... that isn't the one for the file referenced by file path
		if(strcmp(filePathInBase,buff) != 0){
			char descPath[500] = "data/image_nb/base_descripteur_image_nb/";
			char idToString[10];
			sprintf(idToString,"%d",indexId);			

			strcat(descPath,idToString);
			strcat(descPath,".txt");		

			FILE *fichierDescToComp;
			fichierDescToComp = fopen(descPath,"r");

			if(fichierDescToComp == NULL) {
				fclose(fichierListe2);
				fprintf(stderr,"Impossible d'ouvrir le fichier descripteur\n");
				return 1;
			}
			
			int pixelValue;
			int distance = 0;
			
			//Calculate the distance between the two descriptors values
			for(int i=0;i<2;i++){
				fscanf(fichierDescToComp,"%d",&pixelValue);
				int v = pixelValue - descValues[i];
				distance += abs(v);
			}
			
			//If the distance isn't beyond the config limit...
			if(distance <= CONFIG.bwDistanceMax){
				//... add the distance and the filePath to the results
				Result *r = result_init(buff,distance);
				resultArray_addResult(files,r);
			}

			fclose(fichierDescToComp);
		}
	}
	fclose(fichierListe2);
	return 0;
}

int imageBW_play(const char *filePath){
	//Open the file referenced by filePath using eog
	printf("Ouverture %s\n", filePath);
	char command[500] = "eog ";
	strcat(command,filePath);
	strcat(command," &");
	system(command);
	return 0;
}
