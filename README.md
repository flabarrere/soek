# <img src="https://framagit.org/flabarrere/soek/raw/master/assets/icone.svg" alt="Kitten"	title="A cute kitten" width="64" height="64" /> **Soek**


Depuis une vingtaine d’année, le développement d’Internet a révolutionné nos vies quotidiennes et les moteurs de recherches ont été primordiaux dans cette transformation. 

Le projet Fil Rouge 2019-2020 a pour but la réalisation et l'implémentation d’un moteur de recherche simple. Pour ce faire, nous avons monté une équipe de 4 étudiants avec :

*  Guilhem Bouchaud (prépa PSI)
*  Nicolas Clermont-Pezous (DUT Informatique)
*  Florian Labarrère (DUT Informatique)
*  Alexandre Malgouyres (DUT GEII)


