TARGET_EXEC ?= soek
TMP_DIR ?= /tmp/Soek

BUILD_DIR ?= obj
BIN_DIR ?= .
SRC_DIRS ?= src/C
INC_DIRS ?= src/C/include
TST_DIR ?= test


SRCS := $(shell find $(SRC_DIRS) -name *.c -not -name main.c)
OBJS := $(patsubst %.c, %.o, $(SRCS:%=$(BUILD_DIR)/%))
TSTS := $(shell find $(TST_DIR) -name *.c)

CFLAGS := -I$(INC_DIRS) -w

.SILENT:

$(BUILD_DIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@


.PHONY: clean build run test doc data compile

toto :
	echo $(patsubst %.c, %.o, $(SRCS:%=$(BUILD_DIR)/%))

compile: $(OBJS)

build: compile
	mkdir -p $(BIN_DIR)
	$(CC) $(CFLAGS) $(SRC_DIRS)/main.c $(OBJS) -o $(BIN_DIR)/$(TARGET_EXEC) $(LDFLAGS)


test: compile
	mkdir -p $(TMP_DIR)
	$(CC) $(CFLAGS) $(SRCS) $(TSTS) -e test -o $(TMP_DIR)/test_soek
	$(TMP_DIR)/test_soek

doc:
	mkdir -p docs/doxygen
	doxygen .doxyfile

data: data/sample.tar.xz
	tar -xf data/sample.tar.xz -C data/

clean:
	$(RM) -r $(BUILD_DIR)
	$(RM) -r $(shell find data/* -not -name sample.tar.xz)
	$(RM) $(TARGET_EXEC)

